const express = require('express');
const router = express.Router();
const body = require('body-parser');
router.get('/', (req,res)=>{

    res.render('index.html')
    // res.send("<h1> Iniciamos con express </h1>");
    
    })
router.get('/index', (req,res) =>{
    const valores = {
        nboleto:req.query.nboleto,
        destino:req.query.destino,
        ncliente:req.query.ncliente,
        añoscum:req.query.añosc,
        tviaje:req.query.tviaje,
        precio:req.query.precio,
    }
    res.render('index.html',valores);
})

router.post("/index", (req,res) =>{
    const valores = {
        nboleto:req.body.nboleto,
        destino:req.body.destino,
        ncliente:req.body.ncliente,
        añoscum:req.body.añosc,
        tviaje:req.body.tviaje,
        precio:req.body.precio,
    }
    res.render('index.html',valores);
})

module.exports=router;