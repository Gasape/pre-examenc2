const http = require('http');
const express = require('express');
const app = express();
const bodyparser = require("body-parser");
const misrutas = require('./router/index');
const path = require('path');


app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));
app.engine('html',require('ejs').renderFile);

app.use(bodyparser.urlencoded({extended:true}));
app.use(misrutas);

app.use((req,res,next)=>{
    res.status(404).sendFile(__dirname + '/public/error.html')
});

//  Activando el puerto 504 para poder hacer que la aplicacion funcione
const puerto = 504;
app.listen(puerto,() => {
    
    console.log("iniciado en el puerto 504");


});